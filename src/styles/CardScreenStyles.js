import { StyleSheet } from "react-native"

export default StyleSheet.create({
  background: {
    width: '100%',
    height: '100%',
  },
  cardView: {
    padding: 10,
  },
  card: {
    height: '100%',
    borderRadius: 10
  },
  cardImage: {
    height: '100%',
    width: '100%',
  },
  itemStyle: {
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  itemTitle: {
    marginTop: 15,
    fontSize: 22,
    fontWeight: '600'
  },
  itemText: {
    marginTop: 15,
    marginBottom: 25
  },
  imageCover: {
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    overflow: 'hidden',
    height: 300,
    width: '100%'
  },
  modalView: {
    flex: 1,
    marginTop: 50,
    padding: 10
  },
  cardName: {
    fontSize: 20,
    fontWeight: '600',
    marginBottom: 20
  },
  modalImage: {
    height: 250,
    width: '100%'
  },
  descTitle: {
    fontSize: 20,
    fontWeight: '600',
    marginTop: 30,
    marginBottom: 20
  },
  descText: {
    fontSize: 16,
    fontWeight: '500'
  },
  acceptBtn: {
    borderRadius: 10,
    marginTop: 50,
  },
  bottomView: {
    flexDirection: "row",
    flex: 1,
    position: "absolute",
    bottom: 10,
    left: 0,
    right: 0,
    justifyContent: 'space-between',
    padding: 15
  },
  leftBtn: {
    backgroundColor: 'red',
    borderRadius: 10,
    padding: 10
  },
  rightBtn: {
    backgroundColor: '#00cc99',
    borderRadius: 10,
    padding: 10
  }
})
