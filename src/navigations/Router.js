import React, { Component } from 'react'
import { StatusBar } from 'react-native'
import { View } from 'native-base'
import { Router, Scene, Reducer } from 'react-native-router-flux'

import Login from '../screens/Login'
import Register from '../screens/Register'
import ForgotPassword from '../screens/ForgotPassword'
import CardScreen from '../screens/CardScreen'

const reducerCreate = params => {
  const defaultReducer = new Reducer(params)
  return (state, action) => {
    return defaultReducer(state, action)
  }
}
const getSceneStyle = () => ({
  flex: 1,
  backgroundColor: "white",
  elevation: 0,
  shadowOpacity: 0,
  borderBottomWidth: 0,
  shadowColor: null,
  shadowOffset: null,
  shadowOpacity: null,
  shadowRadius: null
})

export default class NavigationRouter extends React.PureComponent {
  render() {
    return (
      <View style={{ flex: 1 }}>
        <StatusBar backgroundColor="black" barStyle="light-content" />
        <Router headerMode="none" createReducer={reducerCreate} getSceneStyle={getSceneStyle}>
          <Scene key="root">
            <Scene key="login" component={Login} hideNavBar={false} panHandlers={null} initial />
            <Scene key="register" component={Register} hideNavBar={false} panHandlers={null} />
            <Scene key="forgot" component={ForgotPassword} hideNavBar={false} panHandlers={null} />
            <Scene key="cardScreen" component={CardScreen} hideNavBar={false} panHandlers={null} />
          </Scene>
        </Router>
      </View>
    )
  }
}
