'use strict';
import React, { Component } from 'react'
import { Root } from "native-base"
import Router from './navigations/Router'
import Pushwoosh from 'pushwoosh-react-native-plugin'

export default class App extends Component {
  componentDidMount() {
    Pushwoosh.init({ 
      "pw_appid" : "1B7A8-64AC7",
      "project_number" : "736098287870"
      // 300CED6E-DC48-462C-80DD-C37344C28144
    });
    Pushwoosh.register();
  }
  
  render() {
    return (
      <Root>
        <Router />
      </Root>
    )
  }
}
