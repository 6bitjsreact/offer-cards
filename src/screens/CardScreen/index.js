import React, { Component } from 'react'
import { Image, ImageBackground } from 'react-native'
import {
  Container,
  Header,
  View,
  DeckSwiper,
  Card,
  CardItem,
  Thumbnail,
  Text,
  Left,
  Body,
  Icon,
  Button
} from 'native-base'
import styles from '../../styles/CardScreenStyles'
import { Modal, TouchableHighlight, Alert } from 'react-native'

const background = require('../../assets/background.png')
const cards = [
  {
    title: 'Offer title can sometimes be two line long',
    text: 'It is a ling established fact that a reader will be distracted by the readable content of a page when looking at its layout',
    image: require('../../assets/image1.jpeg'),
  },
  {
    title: 'This title is only one line',
    text: 'It is a ling established fact that a reader will be distracted by the readable content of a page when looking at its layout',
    image: require('../../assets/image2.jpeg'),
  },
  {
    title: 'One Ward',
    text: 'It is a ling established fact that a reader will be distracted by the readable content of a page when looking at its layout',
    image: require('../../assets/image3.jpeg'),
  },
  {
    title: 'Same case',
    text: 'It is a ling established fact that a reader will be distracted by the readable content of a page when looking at its layout',
    image: require('../../assets/image4.jpeg'),
  }
]

export default class CardScreen extends Component {

  constructor(properties) {
    super(properties)
    this.state = {
      modalVisible: false,
      cardName: '',
      selectedImage: null,
      description: ''
    }
  }

  setModalVisible = (visible, item) => {
    this.setState({
      modalVisible: visible,
      cardName: item.title,
      selectedImage: item.image,
      description: item.text
    })
  }

  render() {
    return (
      <Container>
        <ImageBackground
          source={background}
          style={styles.background}
          resizeMode="stretch"
        >
          <Button transparent style={{ marginTop: 10 }}>
            <Icon style={{ color: 'white', fontSize: 35 }} name='menu' />
          </Button>
          <View style={styles.cardView}>
            <DeckSwiper
              ref={(c) => this._deckSwiper = c}
              dataSource={cards}
              renderItem={item =>
                <Card style={styles.card}>
                  <CardItem
                    cardBody
                    button
                    style={{ borderRadius: 10 }}
                    onPress={() => this.setModalVisible(true, item)}
                  >
                    <View style={styles.imageCover}>
                      <Image style={styles.cardImage} source={item.image} />
                    </View>
                  </CardItem>
                  <CardItem style={styles.itemStyle}>
                    <Text style={styles.itemTitle}>{item.title}</Text>
                    <Text style={styles.itemText}>{item.text}</Text>
                  </CardItem>
                </Card>
              }
            />
            <Modal
              animationType="slide"
              transparent={false}
              visible={this.state.modalVisible}
              onRequestClose={() => {
                Alert.alert('Modal has been closed.')
              }}>
              <View style={styles.modalView} >
                <Text style={styles.cardName} >
                  {this.state.cardName}
                </Text>
                <Image
                  style={styles.modalImage}
                  source={this.state.selectedImage}
                />
                <Text
                  style={styles.descTitle}
                >
                  Description
                </Text>
                <Text style={styles.descText} >
                  { this.state.description }
                </Text>
                <Button
                  onPress={() => this.setState({ modalVisible: !this.state.modalVisible })}
                  style={styles.acceptBtn}
                >
                  <Text>Accept</Text>
                </Button>
              </View>
            </Modal>
          </View>
          <View style={styles.bottomView}>
            <Button
              iconLeft
              style={styles.leftBtn}
              onPress={() => this._deckSwiper._root.swipeLeft()}
            >
              <Text>Not Interested</Text>
            </Button>
            <Button
              iconRight
              style={styles.rightBtn}
              onPress={() => this._deckSwiper._root.swipeRight()}
            >
              <Text>I'm Interested</Text>
            </Button>
          </View>
        </ImageBackground>
      </Container>
    )
  }
}
