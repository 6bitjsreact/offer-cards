import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  KeyboardAvoidingView,
  ImageBackground
} from 'react-native';
import {w, h, totalSize} from '../../styles/Dimensions';
import InputField from '../../components/InputField';
import Continue from './Continue';
import { Actions as NavigationActions } from 'react-native-router-flux'

const email = require('../../assets/email.png');
const password = require('../../assets/password.png');
const repeat = require('../../assets/repeat.png');
const person = require('../../assets/person.png');
const background = require('../../assets/background.png')

export default class Register extends Component {
  state = {
    isNameCorrect: false,
    isEmailCorrect: false,
    isPasswordCorrect: false,
    isRepeatCorrect: false,
    isCreatingAccount: false,
  };

  createUserAccount = () => {
    const name = this.name.getInputValue();
    const email = this.email.getInputValue();
    const password = this.password.getInputValue();
    const repeat = this.repeat.getInputValue();

    this.setState({
      isNameCorrect: name === '',
      isEmailCorrect: email === '',
      isPasswordCorrect: password === '',
      isRepeatCorrect: repeat === '' || repeat !== password,
    })
  };

  changeInputFocus = name => () => {
    switch (name) {
      case 'Name':
        this.setState({ isNameCorrect: this.name.getInputValue() === '' });
        this.email.input.focus();
        break;
      case 'Email':
        this.setState({ isEmailCorrect: this.email.getInputValue() === '' });
        this.password.input.focus();
        break;
      case 'Password':
        this.setState({ isPasswordCorrect: this.password.getInputValue() === '',
          isRepeatCorrect: (this.repeat.getInputValue() !== ''
            && this.repeat.getInputValue() !== this.password.getInputValue()) });
        this.repeat.input.focus();
        break;
      default:
        this.setState({ isRepeatCorrect: (this.repeat.getInputValue() === ''
            || this.repeat.getInputValue() !== this.password.getInputValue()) });
    }
  };

  goBack = () => {
    NavigationActions.pop()
  }

  render() {
    return(
      <KeyboardAvoidingView
        behavior="position"
        keyboardVerticalOffset={-w(40)}
        style={styles.container}
      >
        <ImageBackground
          source={background}
          style={styles.background}
          resizeMode="stretch"

        >
          <View style={styles.regContainer}>
            <Text style={styles.create}>CREATE ACCOUNT</Text>
            <InputField
              placeholder="Name"
              autoCapitalize="words"
              error={this.state.isNameCorrect}
              style={styles.input}
              focus={this.changeInputFocus}
              ref={ref => this.name = ref}
              icon={person}
            />
            <InputField
              placeholder="Email"
              keyboardType="email-address"
              error={this.state.isEmailCorrect}
              style={styles.input}
              focus={this.changeInputFocus}
              ref={ref => this.email = ref}
              icon={email}
            />
            <InputField
              placeholder="Password"
              error={this.state.isPasswordCorrect}
              style={styles.input}
              focus={this.changeInputFocus}
              ref={ref => this.password = ref}
              secureTextEntry={true}
              icon={password}
            />
            <InputField
              placeholder="Repeat Password"
              error={this.state.isRepeatCorrect}
              style={styles.input}
              secureTextEntry={true}
              returnKeyType="done"
              blurOnSubmit={true}
              focus={this.changeInputFocus}
              ref={ref => this.repeat = ref}
              icon={repeat}
            />
            <Continue isCreating={this.state.isCreatingAccount} click={this.createUserAccount}/>
            <TouchableOpacity onPress={this.goBack} style={styles.touchable}>
              <Text style={styles.signIn}>{'<'} Sign In</Text>
            </TouchableOpacity>
          </View>
        </ImageBackground>
      </KeyboardAvoidingView>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: '100%',
    backgroundColor: '#555',
  },
  background: {
    width: '100%',
    height: '100%',
  },
  regContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  create: {
    color:'white',
    fontSize: totalSize(2.4),
    marginTop: h(7),
    marginBottom: h(4),
    fontWeight: '700',
  },
  signIn: {
    color:'#ffffffEE',
    fontSize: totalSize(2),
    fontWeight: '700',
  },
  touchable: {
    alignSelf: 'flex-start',
    marginLeft: w(8),
    marginTop: h(4),
  },
  input: {
    marginVertical: h(2),
  }
});