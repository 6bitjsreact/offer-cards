import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Text,
  Image,
  KeyboardAvoidingView,
  ImageBackground
} from 'react-native';
import InputField from "../../components/InputField";
import {w, h, totalSize} from '../../styles/Dimensions';
import GetStarted from './GetStarted';
import { Actions as NavigationActions } from 'react-native-router-flux'

const companyLogo = require('../../assets/companylogo.png');
const email = require('../../assets/email.png');
const password = require('../../assets/password.png');
const background = require('../../assets/background.png')

export default class Login extends Component {

  state = {
    isEmailCorrect: false,
    isPasswordCorrect: false,
    isLogin: false,
  };

  getStarted = () => {
    const email = this.email.getInputValue();
    const password = this.password.getInputValue();

    this.setState({
      isEmailCorrect: email === '',
      isPasswordCorrect: password === ''
    }, () => {
      if (email !== '' && password !== '') {
        this.setState({ isLogin: true })
        NavigationActions.cardScreen()
      }
    })
  };

  changeInputFocus = name => () => {
    if (name === 'Email') {
      this.setState({ isEmailCorrect: this.email.getInputValue() === '' });
      this.password.input.focus();
    } else {
      this.setState({ isPasswordCorrect: this.password.getInputValue() === '' });
    }
  };

  goToRegister = () => {
    NavigationActions.register()
  }

  goToForgotPassword = () => {
    NavigationActions.forgot()
  }

  render() {
    return (
      <KeyboardAvoidingView
        behavior="position"
        keyboardVerticalOffset={-w(40)}
        style={styles.container}
      >
        <ImageBackground
          source={background}
          style={styles.background}
          resizeMode="stretch"
        >
          <View style={styles.loginContainer}>
            <Image style={styles.icon} resizeMode="contain" source={companyLogo}/>
            <InputField
              placeholder="Email"
              keyboardType="email-address"
              style={styles.email}
              error={this.state.isEmailCorrect}
              focus={this.changeInputFocus}
              ref={ref => this.email = ref}
              icon={email}
            />
            <InputField
              placeholder="Password"
              returnKeyType="done"
              secureTextEntry={true}
              blurOnSubmit={true}
              error={this.state.isPasswordCorrect}
              ref={ref => this.password = ref}
              focus={this.changeInputFocus}
              icon={password}
            />
            <GetStarted
              click={this.getStarted}
              isLogin={this.state.isLogin}
            />
            <View style={styles.textContainer}>
              <TouchableOpacity onPress={this.goToRegister} style={styles.touchable} activeOpacity={0.6}>
                <Text style={styles.createAccount}>Create Account</Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={this.goToForgotPassword} style={styles.touchable} activeOpacity={0.6}>
                <Text style={styles.forgotPassword}>Forgot Password</Text>
              </TouchableOpacity>
            </View>
          </View>
        </ImageBackground>
      </KeyboardAvoidingView>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: '100%',
    backgroundColor: '#555',
  },
  background: {
    width: '100%',
    height: '100%',
  },
  loginContainer: {
    flex: 1,
    alignItems: 'center',
  },
  icon: {
    width: w(70),
    height: h(30),
    marginTop: h(10),
    marginBottom: h(7),
  },
  textContainer: {
    width: w(100),
    flexDirection: 'row',
    marginTop: h(5),
  },
  email: {
    marginBottom: h(4.5),
  },
  touchable: {
    flex: 1,
  },
  createAccount: {
    color:'#ffffffEE',
    textAlign: 'center',
    fontSize: totalSize(2),
    fontWeight: '600',
  },
  forgotPassword: {
    color:'#ffffffEE',
    textAlign: 'center',
    fontSize: totalSize(2),
    fontWeight: '600',
  },
});
